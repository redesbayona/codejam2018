package com.code.jam.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlbf on 31/03/2017.
 */
public class FileUtils {

    public static List<String> readFile(final String filePath, final String fileName) {
        final List<String> strings = new ArrayList<String>();
        String line = null;
        try {
            final FileReader fileReader = new FileReader(filePath+fileName);
            final BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                strings.add(line);
            }
            bufferedReader.close();
        } catch (final Exception e) {

        }
        return strings;
    }

    public static void writeFile(final String filePath, final String fileName,List<String> content) {

        try {
            PrintWriter writer = null;
            writer = new PrintWriter(filePath+fileName, "UTF-8");
            for (String line : content){
                writer.println(line);
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
