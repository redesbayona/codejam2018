package com.code.jam.flights;

import com.code.jam.utils.FileUtils;

import java.util.ArrayList;
import java.util.List;

public class ScrambledItinerary {

    private static final String PATH = "C:\\GFT\\codejam\\codejam2018\\src\\test\\resources\\flights\\";
    private static final String FILE = "large.input";
    private static final String FILE_OUTPUT = "large.output";

    public static void main(String[] args) {

        List<String> lines = FileUtils.readFile(PATH, FILE);
        List<String> result = new ArrayList<String>();

        int cases = Integer.parseInt(lines.get(0));
        int jumpLines = 1;

        for (int numberOfCase = 1; numberOfCase <= cases; ++numberOfCase) {
            int tickets = Integer.parseInt(lines.get(jumpLines));

            Flight[] itinerary = getInitialItinerary(tickets, lines.subList(jumpLines - 1, lines.size()));
            jumpLines += tickets * 2 + 1;
            fixOriginAndDestination(itinerary);

            for (int iterator = 0; iterator < itinerary.length - 1; iterator++) {

                List<Integer> positions = searchFrom(itinerary, itinerary[iterator]);

                if (iterator != positions.get(0) && iterator + 1 != positions.get(0)) {
                    replaceFlight(itinerary, iterator + 1, positions.get(0));
                }
            }

            result.add("Case #" + numberOfCase + ": " + convertObjectArrayToString(itinerary));
        }
        FileUtils.writeFile(PATH, FILE_OUTPUT, result);

    }

    private static Flight[] getInitialItinerary(int tickets, List<String> lines) {
        Flight[] itinerary = new Flight[tickets];
        for (int j = 2; j <= tickets * 2; j = j + 2) {
            int postion = (j - 1) / 2;
            Flight f = new Flight(lines.get(j), lines.get(j + 1), postion);
            itinerary[postion] = f;
        }
        return itinerary;
    }

    private static void fixOriginAndDestination(Flight[] itinerary) {
        int fromUnique = findFromUnique(itinerary);
        replaceFlight(itinerary, 0, fromUnique);
        int toUnique = findToUnique(itinerary);
        replaceFlight(itinerary, itinerary.length - 1, toUnique);
    }

    private static List<Integer> searchFrom(Flight[] itinerary, Flight f1) {
        List<Integer> positions = new ArrayList<Integer>();
        for (int i = 0; i < itinerary.length; i++) {
            if (f1.to.equals(itinerary[i].from) && f1.position != i) {
                positions.add(i);
            }
        }
        return positions;
    }

    private static String convertObjectArrayToString(Flight[] arr) {
        StringBuilder sb = new StringBuilder();
        for (Flight f : arr)
            sb.append(f).append(' ');
        return sb.substring(0, sb.length() - 1);
    }

    private static class Flight {
        String from;
        String to;
        int position;

        Flight(String from, String to, int position) {
            this.from = from;
            this.to = to;
            this.position = position;
        }

        @Override
        public String toString() {
            return this.from + "-" + this.to;
        }

    }

    private static void replaceFlight(Flight[] itinerary, int from, int to) {
        Flight f = itinerary[from];
        f.position = to;
        itinerary[from] = itinerary[to];
        itinerary[from].position = from;
        itinerary[to] = f;
    }

    private static int findFromUnique(Flight[] itinerary) {
        for (int i = 0; i < itinerary.length; i++) {
            boolean unique = true;
            for (int j = 0; j < itinerary.length; j++) {
                if (itinerary[i].from.equals(itinerary[j].to) && i != j) {
                    unique = false;
                }
            }
            if (unique) {
                return i;
            }
        }
        return 0;
    }

    private static int findToUnique(Flight[] itinerary) {
        for (int i = 0; i < itinerary.length; i++) {
            boolean unique = true;
            for (int j = 0; j < itinerary.length; j++) {
                if (itinerary[i].to.equals(itinerary[j].from) && i != j) {
                    unique = false;
                }
            }
            if (unique) {
                return i;
            }
        }
        return 0;
    }
}
